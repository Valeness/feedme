# FeedMe

This script is meant to translate RSS feeds to mastodon posts. Includes interactive CLI to manage the posts, users, and available feeds.

## Installation
Add this script to your mastodon rake file here: `lib/tasks/mastodon.rake`

This is used to create a new user by accepting CLI parameters instead of an interactive interface. (so that users can be created programatically). The created users are not email-verified. So if you ever want to log in as the user, you will need to use an email address domain you have a catch-all tied to. So that you can verify the email of undefineuser@example.com

```
desc 'Add a user by providing their email, username and initial password.' \
   'The user will receive a confirmation email, then they must reset their password before logging in.'
    task add_user2: :environment do
    disable_log_stdout!

    prompt = TTY::Prompt.new

    ARGV.each do|a|
      puts "Argument: #{a}"
    end

    begin
      email = "#{ARGV[1]}@mastodon.instance.com"
      username = ARGV[1]
      role = 'user'
      password = 'supersecurepassword'

      user = User.new(email: email, password: password, admin: role == 'admin', moderator: role == 'moderator', account_attributes: { username: username })

        if user.save
          prompt.ok 'User created and confirmation mail sent to the user\'s email address.'
          prompt.ok "Here is the random password generated for the user: #{user.password}"
        else
          prompt.warn 'User was not created because of the following errors:'

          user.errors.each do |key, val|
            prompt.error "#{key}: #{val}"
          end
      end
    rescue TTY::Reader::InputInterrupt
      prompt.ok 'Aborting. Bye!'
    end
    abort
  end
```
