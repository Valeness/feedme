#!/usr/bin/python

import sys, os, datetime, readline, feedparser, re, hashlib, subprocess
import sqlite3, random

from mastodon import Mastodon
from pprint import pprint

class RssInterface:
   
    def __init__(self):
        self.commands = {}
        self.conn = sqlite3.connect('{0}/feeds.db'.format(os.path.dirname(os.path.abspath(__file__))))
        self.rss = Rss()
        readline.parse_and_bind('tab: complete')

    def add_rss(self):
        name = raw_input('Name: ')
        url = raw_input('Url: ')
        
        subprocess.call(['/home/mastodon/live/new_user.sh', name])

        # Register an App
        app = Mastodon.create_app('RSS Feed', api_base_url = 'https://mastodon.valeness.com', to_file = 'rss_client.secret')
        mastodon = Mastodon(client_id = 'rss_client.secret', api_base_url = 'https://mastodon.valeness.com')
        client = mastodon.log_in('{0}@mastodon.valeness.com'.format(name), 'testpassword123', to_file = 'rss_user.secret')

        client_id = app[0]
        client_secret = app[1]
        access_token = client

        self.rss.insert_user(name, client_id, client_secret, access_token)
        self.rss.insert_feed(name, url)
        
        #mastodon = Mastodon(client_id = client_id, client_secret = client_secret, access_token = access_token, api_base_url = 'https://mastodon.valeness.com')
        #mastodon.toot('testing')
        


    def add_user(self):
        username = raw_input('Username: ')
        client_key = raw_input('Client Key: ')
        client_secret = raw_input('Client Secret: ')
        auth_token = raw_input('Authorization token: ')
        self.rss.insert_user(username, client_key, client_secret, auth_token)

    def add_feed(self):
        #self.add_user()
        name = raw_input("Feed Name: ")
        feed = raw_input("Feed Url: ")
        self.rss.insert_feed(name, feed)
    
    def quitInterface(self):
        Log.write('Goodbnye!')
        sys.exit()

    def add_command(self, name, func):
        self.commands[name] = func

    def run(self):
        self.add_command('add', self.add_feed)
        self.add_command('quit', self.quitInterface)
        self.add_command('create', self.rss.build_schema)
        self.add_command('destroy', self.rss.delete_feeds)
        self.add_command('all', self.rss.get_feeds)
        self.add_command('get', self.rss.get_feed)
        self.add_command('test', self.rss.test_insert)
        self.add_command('toot', self.rss.toot)
        self.add_command('useradd', self.add_user)
        self.add_command('addrss', self.add_rss)

        while(True):
            args = raw_input('Command: ').split(' ')
            readline.add_history(' '.join(args))
            command = args[0]
            params = []
            if(len(args) > 1):
                params = args[1:]
            if command in self.commands:
                self.commands[command](*params)
class Log:
    def __init__(self):
        pass

    @staticmethod
    def write(msg):
        date = datetime.datetime.now()
        print("[{0}] {1}".format(date, msg))

class Rss:

    def __init__(self):
        self.conn = sqlite3.connect('{0}/feeds.db'.format(os.path.dirname(os.path.abspath(__file__))))
        self.conn.row_factory = sqlite3.Row

        self.toot_tries = 0

        pass

    def test_insert(self):

        for feed in self.test_data['feeds']:
            name = feed['name']
            user = self.test_data['users'][name]

            self.insert_feed(name, feed['url'])
            self.insert_user(user['username'], user['client_key'], user['client_secret'], user['access_token'])


    def toot(self, *args):
        c = self.conn.cursor()
        #feed_name = 'nomasters'
        feed_name = c.execute('SELECT * FROM feeds ORDER BY RANDOM() LIMIT 1').fetchone()['name']

        if len(args) > 0:
            feed_name = args[0]

        Log.write(feed_name)
        user = c.execute('SELECT * FROM users WHERE username = \'{0}\''.format(feed_name)).fetchone()
        feed = c.execute('SELECT * FROM feeds WHERE name = \'{0}\''.format(feed_name)).fetchone()
        url = feed['url']

        body = feedparser.parse(url)

        entries = body['entries']

        if len(entries) <= 0:
            Log.write('No entries yo')
            return False
        idx = random.randint(0, len(entries) - 1)

        link = body['entries'][idx]['link']
        title = body['entries'][idx]['title'].encode('utf-8')

        db_key = hashlib.md5()
        db_key.update(title)
        db_key.update(link)
        db_key = db_key.hexdigest()

        c = self.conn.cursor()
        post = c.execute('SELECT * FROM posts WHERE hash = \'{0}\''.format(db_key)).fetchone()

        if not post:

            mastodon = Mastodon(
                client_id = user['client_key'],
                client_secret = user['client_secret'],
                access_token = user['access_token'],
                api_base_url = 'https://mastodon.valeness.com'
            )

            mastodon.toot('{0} - {1}'.format(title, link))
            c.execute('INSERT INTO posts (hash, date) VALUES (\'{0}\', \'{1}\')'.format(db_key, datetime.datetime.now()))
            self.conn.commit()
            Log.write('Tooted about {0}'.format(title))
        else:
            Log.write('{0} has already been posted'.format(title))
            self.toot_tries += 1
            if self.toot_tries < 5:
                self.toot()
            else:
                self.toot_tries = 0


    def insert_user(self, username, client_key, client_secret, access_token):
        c = self.conn.cursor()
        c.execute('INSERT INTO users (username, client_key, client_secret, access_token) VALUES(\'{0}\', \'{1}\', \'{2}\', \'{3}\')'.format(username, client_key, client_secret, access_token))
        self.conn.commit()
        Log.write('Inserted User: {0}'.format(username))

    def delete_user(self, username):
        c = self.conn.cursor()
        c.execute('DELETE FROM users WHERE username = \'{0}\''.format(username))
        c.execute('DELETE FROM feeds WHERE name = \'{0}\''.format(username))
        self.conn.commit()
        Log.write('Delete {0}'.format(username))

    def insert_feed(self, name, feed):
        c = self.conn.cursor()
        c.execute('INSERT INTO feeds (name, url) VALUES (\'{0}\', \'{1}\')'.format(name, feed))
        self.conn.commit()
        Log.write('Inserted Feed: {0}'.format(name))

    def get_feed(self):
        c = self.conn.cursor()
        name = 'reddit_python'
        c.execute('SELECT * FROM feeds WHERE name = \'{0}\''.format(name))
        feed = c.fetchone()
        if(feed):
            print(feedparser.parse(feed['url']))

    def get_feeds(self):
        c = self.conn.cursor()
        c.execute('SELECT * FROM feeds')
        feeds = c.fetchall()
        for i in feeds:
            print(i['name'])
        return feeds

    def delete_feeds(self):
        c = self.conn.cursor()
        c.execute('DROP TABLE IF EXISTS feeds')
        self.conn.commit()
        Log.write('Dropped Table feeds')

    def build_schema(self):
        #self.delete_feeds()
        c = self.conn.cursor();
        c.execute('CREATE TABLE IF NOT EXISTS feeds (name, url)')
        c.execute('CREATE TABLE IF NOT EXISTS users (username, client_key, client_secret, access_token)')
        c.execute('CREATE TABLE IF NOT EXISTS posts (hash, date)')
        self.conn.commit()
        Log.write('Created Table feeds')
        Log.write('Created Table users')
        Log.write('Created Table posts')

if len(sys.argv) > 1 and sys.argv[1] == 'interface':
    interface = RssInterface()
    interface.run()

elif len(sys.argv) > 2 and sys.argv[1] == 'toot':
    rss = Rss()
    rss.toot(sys.argv[2])

else:
    rss = Rss()
    rss.toot()
